import java.util.*;
import java.net.*;
import java.io.*;

/**
 * class RoutingProtocol simulates the routing protocol
 */
class RoutingProtocol extends Thread {
	/**
	 * router simulated by SimRouter
	 */
	SimRouter simrouter;
	String routing;

	/**
	 * update timer duration
	 */
	public static int UPDATE_TIMER_VALUE = 30;

	/**
	 * invalidate timer duration
	 */
	public static int INVALID_TIMER_VALUE = 90;

	// To Do: Declare any other variables required
	RoutingTable mroutiRoutingTable;

	// ----------------------------------------------------------------------------------------------

	/**
	 * @param s
	 *            simulated router
	 */
	public RoutingProtocol(SimRouter s) {
		simrouter = s;
		routing = "";
		mroutiRoutingTable = new RoutingTable();
		// To Do: Do other required initialization tasks.
		start();
	}

	// ------------------------Routing
	// Function-----------------------------------------------
	/**
	 * stores the update data in a shared memory to be processed by the
	 * 'RoutingProtocol' thread later
	 *
	 * @param p
	 *            ByteArray
	 */
	void notifyRouteUpdate(ByteArray p) {// invoked by SimRouter
		// Write code to just to stores the route update data; do not process at
		// this moment, otherwise the simrouter thread will get blocked
	}

	// ----------------------------------------------------------------------------------------------
	/**
	 * update the routing table according to the changed status of an interface;
	 * if interface is UP (status=TRUE), add to routing table, if interface is
	 * down, remove from routing table
	 *
	 * @param interfaceId
	 *            interface id of the router
	 * @param status
	 *            status denoting whether interface is on or off, true if on and
	 *            false otehrwise
	 */
	public void notifyPortStatusChange(int interfaceId, boolean status) {// invoked
																			// by
																			// SimRouter
		// To Do: Update the routing table according to the changed status of an
		// interface. If interface in UP (status=TRUE), add to routing table, if
		// interface is down, remove from routing table
	}

	// ---------------------Forwarding
	// Function------------------------------------------
	/**
	 * returns an NextHop object corresponding the destination IP Address,
	 * dstIP. If route in unknown, return null
	 *
	 * @param destination
	 *            ip
	 * @return returns an NextHop object corresponding the destination IP
	 *         Address if route is known, else returns null
	 */
	NextHop getNextHop(IpAddress dstIp) {// invoked by SimRouter
		// To Do: Write code that returns an NextHop object corresponding the
		// destination IP Address: dstIP. If route in unknown, return null
		for(int i = 0; i < mroutiRoutingTable.mRoutingTableArray.size(); i++)
		{
			
			RoutingTableElement elemnt = mroutiRoutingTable.mRoutingTableArray.get(i);
			IpAddress mIpAddress=elemnt.getmNetworkIpAddress();
			byte mask = (byte) elemnt.mMaskLength;
			IpAddress mnetworkAddress = mIpAddress.getNetworkAddress(mask);
			IpAddress mnetworkAddress1 = dstIp.getNetworkAddress(mask);
			System.out.println("ip address: "+mIpAddress.getString()+" "+mnetworkAddress.getString()+" "+mnetworkAddress1.getString());
			if(mnetworkAddress.sameSubnet(dstIp, mask))
			{
				return new NextHop(dstIp,i+1);

			}
			
			
			
		}
		return null; // default return value
	}

	// -------------------Routing Protocol
	// Thread--------------------------------------
	public void run() {
		// To Do 1: Populate Routing Table with directly connected interfaces
		// using the SimRouter instance. Also print this initial routing table .

		for (int i = 1; i <= simrouter.interfaceCount; i++) {
			if (simrouter.interfaces[i].getIpAddress() != null) {
				IpAddress mIpAddress = simrouter.interfaces[i].getIpAddress();
				//System.out.println("ip address here: "+mIpAddress);
				byte mask = (byte) simrouter.interfaces[i].getSubnetMask();
				IpAddress mnetworkAddress = mIpAddress.getNetworkAddress(mask);
				RoutingTableElement mTableElement = new RoutingTableElement(
						mIpAddress, mask, new NextHop(mIpAddress,simrouter.interfaces[i].interfaceId ),
						"interface", true);

				mroutiRoutingTable.addEntry(mTableElement);
			}
		}
		
		routing = ""+mroutiRoutingTable.printRoutingTable();
		System.out.println(routing);
		

		// To Do 2: Send constructed routing table immediately to all the
		// neighbors. Start the update timer.

		// To Do 3: Continuously check whether there are routing updates
		// received from other neighbours.
		// An update has been received, Now:
		// To Do 3.1: Modify routing table according to the update received.
		// To Do 3.2: Start invalidate timer for each newly added/updated route
		// if any.
		// To Do 3.3:Print the routing table if the routing table has changed
		// To Do Optional 1: Send Triggered update to all the neighbors and
		// reset update timer.
	}

	// ----------------------Timer
	// Handler------------------------------------------------------
	/**
	 * handles what happens when update timer and invalidate timer expires
	 * 
	 * @param type
	 *            of timer: type 1- update timer and type 2- invalid timer
	 *            expired
	 */
	public void handleTimerEvent(int type) {
		// If update timer has expired, then:
		// To Do 1: Sent routing update to all the interfaces. Use
		// simrouter.sendPacketToInterface(update, interfaceId) function to send
		// the updates.
		// To Do Optional 1: Implement split horizon rule while sending update
		// To Do 2: Start the update timer again.

		// Else an invalid timer has expired, then:
		// To Do 3: Delete route from routing table for which invalidate timer
		// has expired.
	}

	// ----------------------------------------------------------------------------------------------

	// ..............................RoutingTableElement..........................................
	class RoutingTableElement {
		private IpAddress mNetworkIpAddress;
		private byte mMaskLength;
		private String mType;
		private boolean mStatus;
		private NextHop mNextHop;

		public void setmNetworkIpAddress(IpAddress mIpAddress) {
			this.mNetworkIpAddress = mIpAddress;
		}

		public void setmMaskLength(byte mMaskLength) {
			this.mMaskLength = mMaskLength;
		}

		public void setmType(String mType) {
			this.mType = mType;
		}

		public void setmStatus(boolean mStatus) {
			this.mStatus = mStatus;
		}


		public void setmNextHop(NextHop mNextHop) {
			this.mNextHop = mNextHop;
		}

		public IpAddress getmNetworkIpAddress() {
			return mNetworkIpAddress;
		}

		public byte getmMaskLength() {
			return mMaskLength;
		}

		public String getmType() {
			return mType;
		}

		public boolean ismStatus() {
			return mStatus;
		}


		public NextHop getmNextHop() {
			return mNextHop;
		}

		public RoutingTableElement(IpAddress ipAddress, byte mask,
				NextHop nextHop, String type, boolean status) {
			mNetworkIpAddress = ipAddress.getNetworkAddress(mask);
			mMaskLength = mask;
			mNextHop = nextHop;
			mType = type;
			mStatus = status;
		}

	}

	// .............................................................................................

	// ......................................Routing Table
	// ..........................................
	class RoutingTable {
		ArrayList<RoutingTableElement> mRoutingTableArray;
		int mSize;
		String routing = "";

		public RoutingTable() {
			mSize = 0;
			mRoutingTableArray = new ArrayList<RoutingTableElement>();
		}

		public void addEntry(RoutingTableElement routingTableElement) {
			mRoutingTableArray.add(mSize, routingTableElement);
			mSize++;
		}

		String printRoutingTable() {

			System.out
					.println("_________________________________________________________________________________________________________________");
			System.out
					.println("|                                                    Routing Table                                               |");

			System.out
					.println("__________________________________________________________________________________________________________________");
			for (int i = 0; i < mSize; i++) {
				if (mRoutingTableArray.get(i).ismStatus()) {
					String nextHop = "";
					if ((mRoutingTableArray.get(i).getmType())
							.equals("interface")) {
						nextHop = ""
								+ mRoutingTableArray.get(i).getmNextHop()
										.getInterfaceId();
					} else if (mRoutingTableArray.get(i).getmType()
							.equals("ip")) {
						nextHop = String.valueOf(mRoutingTableArray.get(i)
								.getmNextHop().getIp());
					}
					// System.out.println("Network :"
					// + mRoutingTableArray.get(i).getmNetworkIpAddress()
					// .getString() + "\tSubnet Mask :"
					// + mRoutingTableArray.get(i).getmMaskLength()
					// + " \tCost :"
					// + mRoutingTableArray.get(i).getmCost()
					// + " \tNext Hop :" + nextHop + " \tType :"
					// + mRoutingTableArray.get(i).getmType());
					routing = routing
							+ "Network :"
							+ mRoutingTableArray.get(i).getmNetworkIpAddress()
									.getString() + "\tSubnet Mask :"
							+ mRoutingTableArray.get(i).getmMaskLength()
							+ " \tNext Hop :" + nextHop + " \tType :"
							+ mRoutingTableArray.get(i).getmType() + "\n";

				}
			}
			return routing;
		}
	}
	// ..............................................................................................
}
