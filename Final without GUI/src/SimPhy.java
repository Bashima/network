import java.util.*;
import java.net.*;
import java.io.*;

/**
 * class SimPhy simulates the physical layer
 */
class SimPhy {
	/**
	 * socket
	 */
	Socket sock;

	/**
	 * input stream
	 */
	DataInputStream br;

	/**
	 * output stream
	 */
	OutputStream bw;

	/**
	 * represents whether connection present
	 */
	boolean hasConnection = false;

	/**
	 * buffer to send bits from physical layer to data link layer
	 */
	Buffer<ByteArray> phy2dll;

	/**
	 * buffer to receive frames from data link layer to physical layer
	 */
	Buffer<ByteArray> dll2phy;

	/**
	 * represents whether the port is up
	 */
	boolean isUp = true;

	/**
	 * @param deviceId
	 *            device id
	 * @param p2d
	 *            buffer for data from physical layer to data link layer
	 * @param d2p
	 *            buffer for data from data link layer to physical layer
	 */
	SimPhy(String deviceId, Buffer<ByteArray> p2d, Buffer<ByteArray> d2p) {
		try {
			sock = new Socket("127.0.0.1", 9009);
			hasConnection = true;

			br = new DataInputStream(sock.getInputStream());

			bw = sock.getOutputStream();

			SimPhy.writeStuffed(bw, deviceId.getBytes());

			phy2dll = p2d;
			dll2phy = d2p;
			new PhySend(this);
			new PhyReceive(this);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * updates port status
	 *
	 * @param s
	 *            boolean representing status of port, true if up and false
	 *            otherwise
	 */
	public void setPortStatus(boolean s) {
		isUp = s;
		// System.out.println("Port Status updated for "+portId+" to "+s);
	}

	/**
	 * returns port status
	 * 
	 * @return true if port is up and false otherwise
	 */
	public boolean getPortStatus() {
		return isUp;
	}

	/**
	 * adds post amble to the stuffed bits
	 */
	public static void writeStuffed(OutputStream bw, byte[] f) throws Exception {
		try {
			byte[] temp = SimPhy.bitStuff(f);
			ByteArray b = new ByteArray(temp.length + 2);
			b.setByteVal(0, (byte) 126);// here there is only post amble.
										// Preamble may be added
			b.setAt(1, temp);
			b.setByteVal(temp.length + 1, (byte) 126);// here there is only post
														// amble. Preamble may
														// be added
			bw.write(b.getBytes());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * 
	 */
	public static byte[] readDeStuffed(DataInputStream br) throws Exception {
		byte[] b = new byte[1000]; // this size is arbitrary.
		int count = 0;
		try {
			byte i = br.readByte();

			while (i != 126) {
			}// skip as long as there is no preamble

			i = br.readByte();
			while (i != 126) {
				b[count++] = i;
				// add code incase count>size of b.
				i = br.readByte();
			}
			byte[] temp = new byte[count];
			System.arraycopy(b, 0, temp, 0, count);

			/* demonstrate the effect of bit stuff */
			return SimPhy.bitDeStuff(temp);
			// return temp;
		} catch (Exception e) {
			e.printStackTrace();
			throw e; // simphy may be invoked from a while(1) loop and everytime
						// there will be an exception. Therefore to avoid it,
						// send some sort of feedback.
		}
	}

	/**
	 * handles bit stuffing
	 */
	

	
//	 public static byte[] bitStuff(byte[] b) throws UnsupportedEncodingException {
//			byte[] temp = new byte[b.length * 2];
//			int size = b.length * 8;
//			int numberOfOne = 0;
//			int tempIndex = 0;
//			for (int i = 0; i < size; i++) {
//	                    //for(int j=0;j<7;j++)
//	                   // int j=0;
//	                  if((b[i/8] &(128>>(i % 8)))!=0)
//	                  {   numberOfOne++;
//	                      temp[tempIndex/8]=(byte) (temp[tempIndex/8]|(128>>(tempIndex % 8)));
//	                            //System.out.println("bit is: :"+1);
//	                  }
//	                   else
//	                   {
//	                            
//						   numberOfOne = 0;
//						   temp[tempIndex / 8] =(byte) (temp[tempIndex / 8] &  (~(128 >>(tempIndex % 8))));
//			
//	                   }
//	                        
//	                   tempIndex++;
//
//				if (numberOfOne == 5) {
//					numberOfOne = 0;
//					// bit stuffing
//					temp[tempIndex / 8] = (byte) (temp[tempIndex / 8] &  (~(128 >>(tempIndex % 8))));
//					tempIndex++;
//				}
//	                                       
//	                    
//	     }
//	                byte[] returnByteArray = new byte[(tempIndex + 7) / 8];
//			System.arraycopy(temp, 0, returnByteArray, 0, returnByteArray.length);
//	                String decoded = new String(returnByteArray, "UTF-8"); 
//	                System.out.println("res1: "+decoded);
//			return returnByteArray;
//				
//				
//	    }
//
	
	public static byte setOne(int i, byte b) {
        b |= (128 >> i);
        return b;
    }

    public static byte setZero(int i, byte b) {
        b &= (byte) (~(128 >> i));
        return b;
    }

    public static int getBit(int pos, byte b) {
        if ((b & (byte) (128 >> pos)) == 0) {
            return 0;
        }
        return 1;
    }
    public static byte[] bitStuff(byte[] b) throws UnsupportedEncodingException {
		// write code for bitstuffing here
		byte[] temp = new byte[b.length * 2];
		int size = b.length * 8;
		int consecutiveOne = 0;
		int countTemp = 0;
		for (int i = 0; i < size; i++) {
			int bit = getBit(i % 8, b[i / 8]);
			if (bit == 1) {
				consecutiveOne++;
				temp[countTemp / 8] = setOne(countTemp % 8,
						temp[countTemp / 8]);
			} else {
				consecutiveOne = 0;
				temp[countTemp / 8] = setZero(countTemp % 8,
						temp[countTemp / 8]);
			}

			countTemp++;

			if (consecutiveOne == 5) {
				consecutiveOne = 0;
				// bit stuffing
				temp[countTemp / 8] = setZero(countTemp % 8,
						temp[countTemp / 8]);
				countTemp++;
			}
		}

		byte[] returnByteArray = new byte[(countTemp + 7) / 8];
		System.arraycopy(temp, 0, returnByteArray, 0, returnByteArray.length);
                 String decoded = new String(returnByteArray, "UTF-8"); 
               // System.out.println("res2: "+decoded);

		return returnByteArray;
		//return b;

	}
	    public static byte[] bitDeStuff(byte[] b) throws UnsupportedEncodingException {
	        //write code for destuffing here
			byte[] temp = new byte[b.length]; //arbitary
		        int tempIndex = 0;
		        int bit, countOne = 0,count_of_zero = 0;
		        for (int i = 0; i < b.length*8; i++) {
		                if((b[i/8] &(128>>(i % 8)))!=0) {
		                    countOne++;
		                    temp[tempIndex/8]=(byte) (temp[tempIndex/8]|(128>>(tempIndex % 8)));
		                }
		                else 
		                {
		                    if (countOne == 5) {
		                        countOne = 0;
		                        count_of_zero++;
		                        continue;
		                    }
		                    countOne = 0;
		                    temp[tempIndex/8] = (byte) (temp[tempIndex/8]&(~(128 >>(tempIndex % 8))));
		                }

		                tempIndex++;
		        }
		      //  System.out.println(count_of_zero);
		        byte[] array = new byte[b.length - (count_of_zero + 7) / 8];
		        System.arraycopy(temp, 0, array, 0, array.length);
	                String decoded = new String(array, "UTF-8"); 
	              //  System.out.println("res1: "+decoded);
		        return array;
			//return b;
	    }


	/**
	 * checks whether port is connected
	 *
	 * @return true if port is connected and false otherwise
	 */
	public boolean connected() {
		return hasConnection;
	}

	/**
	 * receives a frame from data link layer
	 */
	public byte[] receiveFromDll() {
		try {
			synchronized (dll2phy) {
				if (dll2phy.empty())
					dll2phy.wait();
				byte[] f = dll2phy.get().getBytes();
				dll2phy.notify();
				return f;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * sends frame to physical layer
	 *
	 * @param f
	 *            frame to be sent
	 */
	public void sendToLine(byte[] f) throws Exception {
		try {
			SimPhy.writeStuffed(bw, f);
		} catch (Exception e) {
			// e.printStackTrace();
			throw e;
		}
	}

	/**
	 * sends frame to data link layer
	 *
	 * @param f
	 *            frame to be sent
	 */
	public void sendToDll(byte[] f) {
		try {
			synchronized (phy2dll) {
				if (phy2dll.full())
					phy2dll.wait();
				ByteArray b = new ByteArray(f);
				phy2dll.store(b);
				phy2dll.notify();
				System.out.println("physical sedning to DLL");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * receives bits from physical layer, converts the bits to frame and returns
	 * the converted frame
	 *
	 * @return frame formed from bits from physical layer
	 */
	public byte[] receiveFromLine() throws Exception {
		try {
			byte[] f = readDeStuffed(br);
			return f;
		} catch (Exception e) {
			// e.printStackTrace(); //not required. already printed by simphy
			throw e;
		}
	}

	/**
	 * class PhySend receives frame from data link layer and sends it to
	 * physical layer
	 */
	private class PhySend extends Thread {
		SimPhy simphy;

		public PhySend(SimPhy s) {
			simphy = s;
			start();
		}

		public void run() {
			try {
				while (true) {
					byte[] f = receiveFromDll();
					sendToLine(f);
				}
			} catch (Exception e) {
			}
		}
	}

	/**
	 * class PhyReceive receives frame from physical layer and sends it to data
	 * link layer
	 */
	private class PhyReceive extends Thread {
		SimPhy simphy;

		public PhyReceive(SimPhy s) {
			simphy = s;
			start();
		}

		public void run() {
			try {
				while (true) {
					byte[] f = receiveFromLine();
					if (simphy.getPortStatus()) {
						if (f != null)
							sendToDll(f);
					}
				}
			} catch (Exception e) {
			}
		}
	}
	// =======================================================
}
