import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.Timer;


/**
 * class RoutingProtocol simulates the routing protocol
 */
class RoutingProtocol extends Thread{
	/**
	 * router simulated by SimRouter
	 */
	 Timer upTimer;
	SimRouter simrouter;
	Timer t;
	Map<RoutingTableElement, Timer> invalidTimerEntryMap;
	/**
	 * update timer duration
	 */
	//public static int UPDATE_TIMER_VALUE=30;
	public static int UPDATE_TIMER_VALUE=15;
	/**
	 * invalidate timer duration
	 */
	//public static int INVALID_TIMER_VALUE=90;
        public static int INVALID_TIMER_VALUE=90;
	//To Do: Declare any other variables required
        RoutingTable routingTable;
         Buffer<ByteArray> bufferUpdate;
         boolean metaphore= true;
	//----------------------------------------------------------------------------------------------

	/**
	 * @param s simulated router
	 */
	public RoutingProtocol(SimRouter s){
		simrouter=s;

		//To Do: Do other required initialization tasks.
        routingTable = new RoutingTable();
       bufferUpdate = new Buffer<ByteArray>("UpdateBuffer", 100);
       invalidTimerEntryMap=new HashMap<RoutingTableElement, Timer>();
		start();
                //Timer timer = new Timer(500, this);
	}

	//------------------------Routing Function-----------------------------------------------
	/**
	 * stores the update data in a shared memory to be processed by the 'RoutingProtocol' thread later
	 *
	 * @param p ByteArray
	 */
	void notifyRouteUpdate(ByteArray p){//invoked by SimRouter
		//Write code to just to stores the route update data; do not process at this moment, otherwise the simrouter thread will get blocked
                try {
            synchronized (bufferUpdate) {
                if (bufferUpdate.full()) {
                    return;
                } else {
                    bufferUpdate.store(p);
                }
                bufferUpdate.notify();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        }
	//----------------------------------------------------------------------------------------------
	/**
	 * update the routing table according to the changed status of an interface; if interface is UP (status=TRUE), add to routing table, if interface is down, remove from routing table
	 *
	 * @param interfaceId interface id of the router
	 * @param status status denoting whether interface is on or off, true if on and false otehrwise
	 */
	public void notifyPortStatusChange(int interfaceId, boolean status){//invoked by SimRouter
		//To Do: Update the routing table according to the changed status of an interface. If interface in UP (status=TRUE), add to routing table, if interface is down, remove from routing table
            simrouter.interfaces[interfaceId].setPortStatus(status);
            IpAddress portIp = simrouter.interfaces[interfaceId].getIpAddress();
            IpAddress subNetIp = portIp.getNetworkAddress(simrouter.interfaces[interfaceId].subnetMask);
            for (int i = 0; i < routingTable.mSize; i++) {
//            	System.out.println("i: "+i);
                if (routingTable.mRoutingTableArray[i].mNetworkIpAddress.sameIp(subNetIp)) {
                    routingTable.mRoutingTableArray[i].setmStatus(status);
                    System.out.println("here disable "+i);
                    break;
                }
            }
            System.out.println("Port Status Changed: Updated Routing Table: ");
            routingTable.printRoutingTable();


        }

	//---------------------Forwarding Function------------------------------------------
	/**
	 * returns an NextHop object corresponding the destination IP Address, dstIP. If route in unknown, return null
	 *
	 * @param destination ip
	 * @return returns an NextHop object corresponding the destination IP Address if route is known, else returns null
	 */
	NextHop getNextHop(IpAddress dstIp){//invoked by SimRouter
		//To Do: Write code that  returns an NextHop object corresponding the destination IP Address: dstIP. If route in unknown, return null
                NextHop nexthop=null;
                int size = routingTable.mSize;
                for(int i=0;i<size;i++){
                    IpAddress network = dstIp.getNetworkAddress((int) routingTable.mRoutingTableArray[i].mMaskLength);
                   for(int j=0; j<size; ++j){
                       if(network.sameIp(routingTable.mRoutingTableArray[j].mNetworkIpAddress)){
                           if( routingTable.mRoutingTableArray[j].mType.equals("interface")){
                        	   nexthop=new NextHop(dstIp,routingTable.mRoutingTableArray[j].mNextHop.interfaceId);
                              
                           break;
                           }
                        else if(routingTable.mRoutingTableArray[j].mType.equals("ip")){
                        	nexthop=routingTable.mRoutingTableArray[j].mNextHop;
                            System.out.println("nexthop:"+nexthop.ip.getString()+" interface id:"+nexthop.interfaceId);
                        }


                        }
                   }
               }
            return nexthop; 
	}
	
  
	//-------------------Routing Protocol Thread--------------------------------------
	public void run(){
		//To Do 1: Populate Routing Table with directly connected interfaces using the SimRouter instance. Also print this initial routing table	.
		for (int i = 1; i <= simrouter.interfaceCount; i++) {
                    if(simrouter.interfaces[i].isConfigured){
                    	 byte mask = (byte) simrouter.interfaces[i].subnetMask;
                        IpAddress ip =simrouter.interfaces[i].ipAddr;
                        IpAddress ipNetAddress = ip.getNetworkAddress(mask);
                        int tempId=simrouter.interfaces[i].interfaceId;
                        NextHop nxthop=new NextHop(ip,tempId);
                        RoutingTableElement temp= routingTable.addEntry(ipNetAddress,mask,(byte) 0,nxthop,"interface",0);
                        startInvalidTimer(temp);
                       
                        
                    }
                }
                routingTable.printRoutingTable();
             
      
		//To Do 2: Send constructed routing table immediately to all the neighbors. Start the update timer.
		for (int i = 1; i <= simrouter.interfaceCount; i++)
                {
                    Packet p;
                    if(simrouter.interfaces[i].isConfigured){     
                     ByteArray tmp = routingTable.tableToBytes(simrouter, i);
                     simrouter.sendPacketToInterface(tmp, i);

                    }
                }

                
                //TimerTask obj=new timerCustom(1, this);
                upTimer=new Timer(UPDATE_TIMER_VALUE * 1000, updatePerformer);
                upTimer.start();
         		//timer.scheduleAtFixedRate(obj, UPDATE_TIMER_VALUE, UPDATE_TIMER_VALUE);


		//To Do 3: Continuously check whether there are routing updates received from other neighbours.

        ByteArray recievedPacket;
        try {

            while (true) {
                synchronized (bufferUpdate) {
                    if (bufferUpdate.empty()) {
                        bufferUpdate.wait();
                    }
                    recievedPacket =bufferUpdate.get();
                   bufferUpdate.notify();
                }
         //An update has been received, Now:
        //To Do 3.1: Modify routing table according to the update received.
                int s = 8;
                byte getMsg[] = new byte[4];
                System.arraycopy(recievedPacket.getBytes(), 0, getMsg, 0, 4);
                IpAddress getSrcIp = new IpAddress(getMsg);
                System.arraycopy(recievedPacket.getBytes(), 4, getMsg, 0, 4);
                IpAddress getDestIp = new IpAddress(getMsg);
                getMsg = new byte[6];

                try {
                    for (int i = 0;; i++) {
                        System.arraycopy(recievedPacket.getBytes(), s, getMsg, 0, 6);
                        s += 6;
                        IpAddress netIpAdres = new IpAddress(getMsg); 
                        byte getMask = getMsg[4];
                        byte getCost = getMsg[5];
                        int ret=routingTable.checkAddNewEntry(netIpAdres, getMask, getCost, getSrcIp, this,0);
                        if(ret==1)
                        {
                        	////restart timer
                        	RoutingTableElement element=routingTable.findTableEntry(netIpAdres,getMask);
                        	reStartInvalidTimer(element);
                        }
                        else if(ret==2)
                        {
                        	//start timer
                        	RoutingTableElement element=routingTable.findTableEntry(netIpAdres,getMask);
                        	startInvalidTimer(element);
                        	
                        }
                        System.out.println("loop :" + i);
                    }
                } catch (Exception ae) {
                    System.out.println("Update found:\n");
                }
                //To Do 3.2: Start invalidate timer for each newly added/updated route if any.
		//To Do 3.3:Print the routing table if the routing table has changed
                 routingTable.printRoutingTable();
		//To Do Optional 1: Send Triggered update to all the neighbors and reset update timer.
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        }

	//----------------------Timer Handler------------------------------------------------------
	/**
	 * handles what happens when update timer and invalidate timer expires
	 *
	 * @param type of timer: type 1- update timer and type 2- invalid timer expired
	 */
	ActionListener updatePerformer = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Up timer finished");
				handleTimerEvent(1); // 1 for update event
				
			}

			
		};
		
		ActionListener periodicInvalidation = new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("Invalid timer finished!!!!!");
				handleTimerEvent(2); // 2 for invalid event
				
			}
		};

	public void handleTimerEvent(int type){
		//If update timer has expired, then:
			//To Do 1: Sent routing update to all the interfaces. Use simrouter.sendPacketToInterface(update, interfaceId) function to  send the updates.
			try
			{
				if (type == 1) {
	                for (int i = 1; i <= simrouter.interfaceCount; i++) {
	                     if(simrouter.interfaces[i].isConfigured){
	                    	 System.out.println("i status: "+i+" "+routingTable.mRoutingTableArray[i].mStatus);
	                        if(routingTable.mRoutingTableArray[i].mStatus) {
	                            ByteArray pckt = routingTable.tableToBytes(simrouter, i);
	                            simrouter.sendPacketToInterface(pckt, i);
	                        }
	                    }
	                 }
	                upTimer.restart();
	            }

			//Else an invalid timer has expired, then:
				//To Do 3:  Delete route from routing table for which invalidate timer has expired.
	            else if(type==2){
	            	
	            	boolean isDeleted = removeInvalidEntry();
//	            	System.out.println("removing :"+isDeleted);
	    			if(isDeleted) {
	    				routingTable.printRoutingTable();
	    			}

	            }
			}catch(Exception ex)
				{
					
				}
            
        }
	
	public void startInvalidTimer(RoutingTableElement tableElement) {
		
		Timer invalidTimer = new Timer(INVALID_TIMER_VALUE * 1000, periodicInvalidation);
		invalidTimer.setRepeats(false);
		invalidTimer.start();
		invalidTimerEntryMap.put(tableElement, invalidTimer);
	}
	
public void reStartInvalidTimer(RoutingTableElement tableElement) {
		
	
	invalidTimerEntryMap.remove(tableElement);
		startInvalidTimer(tableElement);
	}
public boolean removeInvalidEntry()
{
	boolean updated=false;
	Iterator<Map.Entry<RoutingTableElement, Timer>> iterator = 
			invalidTimerEntryMap.entrySet().iterator();
	try{
		while(iterator.hasNext()) {
//			System.out.println("delete hobe");
			Map.Entry<RoutingTableElement, Timer> entry = iterator.next();
			if(entry.getValue().isRunning() == false) {
//				System.out.println("delete hobe false e asche :"+entry.getKey().mType);
				RoutingTableElement tableElement = entry.getKey();
				
				//if(tableElement.mType.equals("ip")){
					{System.out.println("deleted: "+tableElement.mNetworkIpAddress.getString());	
					routingTable.removeEntry(tableElement);	
					iterator.remove();
					updated = true;
				}
			}
		}
	}catch(Exception ex){ex.printStackTrace();}
//	System.out.println("delete ki hoise: "+updated);
	return updated;
	
}
	//----------------------------------------------------------------------------------------------
}
class RoutingTableElement{

    IpAddress mNetworkIpAddress;
    NextHop mNextHop;
    byte mMaskLength;
    byte cost;
    String mType;
    boolean mStatus;
  //  newTimer t;
    public RoutingTableElement(IpAddress netIp,byte mLength,NextHop nextHop,byte cost,String networkType,boolean flag){
        this.mNetworkIpAddress=netIp;
        this.mNextHop=nextHop;
        this.mMaskLength=mLength;
        this.cost=cost;
        this.mType=networkType;
        this.mStatus=flag;
    }
    void changeFlag(boolean flag){
        this.mStatus=flag;
    }
    public void setmNetworkIpAddress(IpAddress mIpAddress) {
		this.mNetworkIpAddress = mIpAddress;
	}

	public void setmMaskLength(byte mMaskLength) {
		this.mMaskLength = mMaskLength;
	}

	public void setmType(String mType) {
		this.mType = mType;
	}

	public void setmStatus(boolean mStatus) {
		this.mStatus = mStatus;
	}


	public void setmNextHop(NextHop mNextHop) {
		this.mNextHop = mNextHop;
	}

	public IpAddress getmNetworkIpAddress() {
		return mNetworkIpAddress;
	}

	public byte getmMaskLength() {
		return mMaskLength;
	}

	public String getmType() {
		return mType;
	}

	public boolean ismStatus() {
		return mStatus;
	}


	public NextHop getmNextHop() {
		return mNextHop;
	}

	public RoutingTableElement(IpAddress ipAddress, byte mask,
			NextHop nextHop, String type, boolean status) {
		mNetworkIpAddress = ipAddress.getNetworkAddress(mask);
		mMaskLength = mask;
		mNextHop = nextHop;
		mType = type;
		mStatus = status;
	}
	public RoutingTableElement(IpAddress ipAddress, byte mask,
			NextHop nextHop, String type, boolean status,byte cost) {
		mNetworkIpAddress = ipAddress.getNetworkAddress(mask);
		mMaskLength = mask;
		mNextHop = nextHop;
		mType = type;
		mStatus = status;
		this.cost=cost;
	}


}

class RoutingTable{
    RoutingTableElement mRoutingTableArray[];
    int mSize;
    String s;
    public RoutingTable(){
        mRoutingTableArray= new RoutingTableElement[99];
         mSize=0;
    }
   
    public RoutingTableElement addEntry(IpAddress netIp, byte maskLenght, byte cost, NextHop nextHop, String netType,int ret){
    	RoutingTableElement temp=new RoutingTableElement(netIp, maskLenght, nextHop, cost, netType, true);
    	mRoutingTableArray[mSize]= temp;
        mSize++;
        System.out.println("adding: "+(mSize-1));
        return temp;
    }
    
    public void addEntry(IpAddress netIp, byte maskLenght, byte cost, NextHop nextHop, String netType){
        mRoutingTableArray[mSize]= new RoutingTableElement(netIp, maskLenght, nextHop, cost, netType, true);
        mSize++;
        System.out.println("adding: "+(mSize-1));
    }
//    public void addTimer(RoutingProtocol rtp){
//        mRoutingTableArray[mSize-1].t= new newTimer(rtp,2,rtp.INVALID_TIMER_VALUE);
//        mRoutingTableArray[mSize-1].t.status= true;
//    }
//    public void addTimerStart(RoutingProtocol rtp){
//        mRoutingTableArray[mSize-1].t= new newTimer(rtp,2,rtp.INVALID_TIMER_VALUE);
//        System.out.println("update timer started for member :"+(mSize-1));
//        mRoutingTableArray[mSize-1].t.startTimer();
//    }
    public int activeSize()
    {
      int activeRouters=0;
      for (int i = 0; i < mSize; i++) {
          if (mRoutingTableArray[i].mStatus==true)
        	  activeRouters++;
      }
      return activeRouters;
    	
    }
    ByteArray tableToBytes(SimRouter s, int port) {
    	int actives=activeSize();
        byte[] payload = new byte[6 * actives];
        int pos = 0;
        for (int i = 0; i < mSize; i++) {
            if (mRoutingTableArray[i].mStatus) {
                System.arraycopy(mRoutingTableArray[i].mNetworkIpAddress.getBytes(), 0, payload, pos, 4);
                 pos+= 4;
                payload[ pos++] = mRoutingTableArray[i].mMaskLength;
                payload[ pos++] = mRoutingTableArray[i].cost;
            }
        }
        Packet p = new Packet(s.interfaces[port].getIpAddress(), new IpAddress("224.0.0.0"), payload);
        ByteArray tablesend = new ByteArray(p.getBytes().length + 1);
        tablesend.setByteVal(0, (byte) 255);
        tablesend.setAt(1, p.getBytes());
        return tablesend;
    }
	public RoutingTableElement findTableEntry(IpAddress network, byte mask) {
			
			for (int i = 0; i < mRoutingTableArray.length; i++) {
				
				if (mRoutingTableArray[i].mNetworkIpAddress.sameSubnet(network, mask)) {
					return mRoutingTableArray[i];
				}
			}
			
			return null;
		}



    void printRoutingTable(){
        s="";
        System.out.println("\nSHOWING ROUTING TABLE: \n");
        //String nxtHop = null ;
        for(int i=0;i<mSize;i++){
            if(mRoutingTableArray[i].mStatus) {
            String nxtHop = null ;
             if ((mRoutingTableArray[i].mType).equals("interface")) {
                   nxtHop = "" + mRoutingTableArray[i].mNextHop.getInterfaceId();

                } else if (mRoutingTableArray[i].mType.equals("ip")) {
                    nxtHop = mRoutingTableArray[i].mNextHop.getIp().getString();
                }
            s+="Network Ip address:" + mRoutingTableArray[i].mNetworkIpAddress.getString() +
                        "; \nSubnet Mask Length :" + mRoutingTableArray[i].mMaskLength
                        + " \nCost :" + mRoutingTableArray[i].cost + " \nNext Hop :" + nxtHop+ " \nType :" + mRoutingTableArray[i].mType+"\n\n\n";
                System.out.println("Network Ip address:" + mRoutingTableArray[i].mNetworkIpAddress.getString() +
                        "; Subnet Mask Length :" + mRoutingTableArray[i].mMaskLength
                        + " Cost :" + mRoutingTableArray[i].cost + " Next Hop :" + nxtHop+ " Type :" + mRoutingTableArray[i].mType+"\n");

            }
        }
//        System.out.println("\n");
        System.out.println("\n");
    }
    void checkAddNewEntry(IpAddress network, byte mask, byte cost, IpAddress srcIp, RoutingProtocol rtp) {
        int n = mSize;
        int interfaceId = 0;
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            //System.out.println("here"+network.getString()+"-"+rtable[i].networkAddress.getString());
            if (mRoutingTableArray[i].mNetworkIpAddress.sameSubnet(network, mask)) {
                flag = false;
                
            }

        }
        if (flag) {
            int cst = (int) cost;
            cst++;
            for (int i = 1; i <= rtp.simrouter.interfaceCount; i++) {
                 if(rtp.simrouter.interfaces[i].isConfigured){
                    if (srcIp.sameSubnet(rtp.simrouter.interfaces[i].getIpAddress(), mask)) {
                        interfaceId = i;
                        break;
                    }
                }

            }
            addEntry(network, mask, (byte) cst, new NextHop(srcIp, interfaceId), "ip");
//            addTimerStart(rtp);
        }

    }
    public int  checkAddNewEntry(IpAddress network, byte mask, byte cost, IpAddress srcIp, RoutingProtocol rtp,int typ) {
        int n = mSize;
        int interfaceId = 0;
        boolean flag = true;

        for (int i = 0; i < n; i++) {
            //System.out.println("here"+network.getString()+"-"+rtable[i].networkAddress.getString());
            if (mRoutingTableArray[i].mNetworkIpAddress.sameSubnet(network, mask)) {
                flag = false;
                return 1;
            }

        }
        if (flag) {
            int cst = (int) cost;
            cst++;
            for (int i = 1; i <= rtp.simrouter.interfaceCount; i++) {
                 if(rtp.simrouter.interfaces[i].isConfigured){
                    if (srcIp.sameSubnet(rtp.simrouter.interfaces[i].getIpAddress(), mask)) {
                        interfaceId = i;
                        break;
                    }
                }

            }
            addEntry(network, mask, (byte) cst, new NextHop(srcIp, interfaceId), "ip");
//            addTimerStart(rtp);
            return 2;
        }
        return 0;

    }
    public void removeEntry(RoutingTableElement element)
    {
    	for (int i = 0; i < mRoutingTableArray.length; i++) {
			
			if (mRoutingTableArray[i].mNetworkIpAddress.sameSubnet(element.mNetworkIpAddress, element.mMaskLength)) {
				for(int j=i+1;j<mSize;j++){
					mRoutingTableArray[j-1]=mRoutingTableArray[j];
              }
              mSize--;
			
			}
			break;
		}
    }

}

